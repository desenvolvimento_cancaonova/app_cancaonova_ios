//
//  PostViewCell.swift
//  cancaonova
//
//  Created by Desenvolvimento on 03/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit
import UICircularProgressRing


class PostViewCell: UITableViewCell {
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblChannel: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    
    @IBOutlet weak var btShare: UIButton!
    @IBOutlet weak var btReader: UIButton!
    @IBOutlet weak var btLike: UIButton!
    @IBOutlet weak var tapView: UIView!
   
    
    @IBOutlet weak var lblDescription: UILabel!
    //@IBOutlet weak var cpDonation: UICircularProgressRingView!
    
    var share:Bool  = false
    var like:Bool   = false
    var reader:Bool = false
    var post: Post!
    var indexPath: IndexPath!
    var sender: UIButton!
    var blurEffect:UIBlurEffect!
    
    func changeState(state: Bool, sender: Any){
        DispatchQueue.main.async {
            let bt: UIButton = sender as! UIButton
            bt.isHighlighted = state
        }
    }
    
    func setPost(post:Post){
        self.post = post
        
        self.checkLikeButtonState()
        self.checkReaderButtonState()
        
    }
    
    func checkLikeButtonState(){
        
        let lc:LikeController = LikeController.init(post: post, lblLike: self.lblLike)
        self.like = lc.isActived()
        self.changeState(state: self.like,sender: btLike)
        
    }
    
    
    func checkReaderButtonState(){
        
        
        if((btReader) != nil){
            //TODO remover o acesso direto ao dao 
            let readerdao:ReaderDAO = ReaderDAO.init( )
            
            self.reader = readerdao.isActived (model: post)
           // readerdao.closeConnection()
                    
            self.changeState( state: self.reader, sender: btReader)
        }

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapViewGesture = UITapGestureRecognizer(target: self, action: #selector(tapTitle(gesture:)))
        self.tapView.addGestureRecognizer(tapViewGesture)
    }
    
    
    @IBAction func btLikeAction(sender: UIButton) {
        
        
        var strAction:String = ""
        
        if (self.like == false){
            self.like = true
            strAction = "like"
        }else{
            self.like = false
            strAction = "dislike"
        }
        
        changeState(state: self.like,sender: self.btLike)
        
        let lc:LikeController = LikeController.init(post: self.post, lblLike: lblLike)
        lc.sendLike(type: strAction,  indexPath: self.indexPath, sender: sender)
 
    }
    
    
    @IBAction func btShareAction(sender: UIButton) {
        
        DispatchQueue.main.async(execute: {
            sender.isEnabled = false
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.enableButton), userInfo: nil, repeats: false)
        })
        
        let infoData:[String: NSObject] = ["post": post, "image":imgThumb.image!]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "shareNotification"), object: nil, userInfo: infoData)
        
    }
    
    @IBAction func btReaderAction(sender: UIButton) {
        let rc:ReaderController = ReaderController()
        rc.save(post: self.post, indexPath: self.indexPath, sender: sender)
    }
    
    
    @objc func enableButton() {
        btShare.isEnabled = true
    }
    @objc  
    func tapTitle(gesture:  UIGestureRecognizer){
        actionPost()
    }
    
    func actionPost(){
        let infoData:[String: Any] = ["post": post, "indexPath" : indexPath]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "actionNotification"), object: nil, userInfo: infoData)
    }
    
}
