//
//  URLDAO.swift
//  cancaonova
//
//  Created by Desenvolvimento on 30/08/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import CouchbaseLite

class URLDAO:BaseDAO{
    
    var postDonate:Post!
    var urlValue: URL!
    
    init() {
        super.init(dataBase: "config")
    }
    
    func getURLFromCB(urlName:String) -> URL{
        
        do{
            
            let query =  try CBLQueryBuilder (database: database, select: ["id", "title", "content" ],  where: "source_slug='\(urlName)'",orderBy: ["-date"])
            
            let queryObj = query.createQuery(withContext: nil)
            queryObj.limit = 1
            
            let result = try! queryObj.run()
            
            if( result.count > 0 ){
                
                let row:CBLQueryRow = result.row(at: 0)
                postDonate = Post(for: (row.document)!)!
                
                urlValue = NSURL(string: postDonate.content) as URL!
            }else{
                
                urlValue = getURLFromUserDefinedSettings(urlName: urlName)
            }
            
        }catch{
            
            urlValue = getURLFromUserDefinedSettings(urlName: urlName)
            
        }
        
        
        if (urlName != "prayer_to") {
            self.closeConnection()
        }
        
        
        return urlValue
        
        
    }
    
    func getURLFromUserDefinedSettings(urlName: String)->URL{
        
        let bundleDictionary = Bundle.main .infoDictionary!
        let urlDefinitionsInBundle = bundleDictionary["URLValues"] as! NSDictionary
        let urlValue: URL = NSURL(string: urlDefinitionsInBundle.value(forKey: urlName)! as! String) as URL!
        
        return urlValue
    }
    
    
}
