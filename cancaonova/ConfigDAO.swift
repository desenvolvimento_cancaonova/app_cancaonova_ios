//
//  ConfigDAO.swift
//  cancaonova
//
//  Created by Desenvolvimento on 31/08/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import CouchbaseLite

class ConfigDAO:BaseDAO{
    
    
    var puller: CBLReplication!
    
    init() {
        super.init(dataBase: "config")
    }
    
    func startPuller(){
        
        let urlDAO: URLDAO = URLDAO.init()
        let url:URL = urlDAO.getURLFromUserDefinedSettings(urlName: "url")
       // let url:URL = urlDAO.getURLFromCB(urlName: "url")
        puller = database.createPullReplication(url)
        puller.continuous = true
        //puller.channels = ["app-cn"]
        puller.channels = ["config"]
        var auth: CBLAuthenticatorProtocol?
        auth = CBLAuthenticator.basicAuthenticator(withName: "appcn", password: "#q1w2e3#")
        puller.authenticator = auth
        puller.start()
    }

    
}
