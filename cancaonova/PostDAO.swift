//
//  PostDAO.swift
//  cancaonova
//
//  Created by Desenvolvimento on 10/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation

import Foundation
import CouchbaseLite

class PostDAO: BaseDAO {
    init(){
        super.init(dataBase: "timeline")
    }
    
    
    func countPosts()->Int{
        
        let query =  try!  CBLQueryBuilder (database: database, select: ["id","title", "content","date","action", "type", "widget", "source", "source_slug", "image_url"],where: "1=1", orderBy: ["-date"])
        
        let queryObj  = query.createQuery(withContext: nil)
        
        let result = try! queryObj.run()
        
        return Int(result.count)
        
    }
    
    
    
   
}
