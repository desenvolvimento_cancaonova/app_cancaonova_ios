//
//  AboutNotificationCell.swift
//  cancaonova
//
//  Created by Desenvolvimento on 11/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit
import OneSignal

class AboutNotificationCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var swiOnesignal: UISwitch!
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        
        swiOnesignal.setOn(status.subscriptionStatus.subscribed, animated: false)
        
    }
     
    
    @IBAction func switchValueDidChange(sender:UISwitch!)
    {
        OneSignal.setSubscription(sender.isOn)
    }
}
