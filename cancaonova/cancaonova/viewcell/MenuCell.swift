//
//  PostHeaderCell.swift
//  cancaonova
//
//  Created by Desenvolvimento on 05/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!

}
