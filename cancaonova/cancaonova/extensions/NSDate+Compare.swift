//
//  NSDate+Compare.swift
//  cancaonova
//
//  Created by Desenvolvimento on 03/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation



extension NSDate {
    
    
    func timeOfOccurrence (date date1: NSDate, andDate date2: NSDate) ->String{
        
        var seconds = Int(date1.timeIntervalSince(date2 as Date))
        var  diff: String = ""
        
        let days =  seconds / (3600 * 24);
        diff = "Há menos de um minuto"
        
        if(seconds > 0){
            
            if(days>0){
                seconds -= days * 3600 * 24;
                diff = String(format: "Há %i dias", days)
            }else{
                let hours =  seconds / 3600;
                
                if(hours > 0) {
                    seconds -= hours * 3600;
                    diff = String(format: "Há %i horas", hours)
                }else{
                    let minutes =  seconds / 60;
                    
                    if(minutes > 0){                        
                        diff = String(format: "Há %i minutos", minutes)
                    }
                    
                }
            }
        }
        
        return diff
    }
}
