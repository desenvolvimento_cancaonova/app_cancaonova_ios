//
//  UIButton+ExpandArea.swift
//  cancaonova
//
//  Created by Desenvolvimento on 23/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    
    override open func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let relativeFrame = self.bounds
        let hitEdgeInsets = UIEdgeInsetsMake(-15, -15, -15, -15)
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitEdgeInsets)
        return hitFrame.contains(point)
    }
    
    
}
