//
//  File.swift
//  cancaonova
//
//  Created by Desenvolvimento on 10/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import CouchbaseLite


class PostController{
    var postDao:PostDAO!
    
    
    init(){
         postDao = PostDAO.init()
    }
    
    
    func countPosts()->Int{
       // let postDao = PostDAO.init()
        let r: Int = postDao.countPosts()
        return r
    }
    
    
    func select(documentID:String)->Post{
        let doc = postDao.select(documentID: documentID)
        let post:Post = Post(for: (doc)!)!
        return post
    }
    
    func  select( lastKey:Any!)-> CBLQueryEnumerator {
        let query: CBLQueryEnumerator = postDao.select(lastKey: lastKey)
        return query
    }
    
    
    func createPuller()->CBLReplication{
        let urlDAO: URLDAO = URLDAO.init()
        let url:URL = urlDAO.getURLFromCB(urlName: "url")
        let puller:CBLReplication = postDao.database.createPullReplication(url)
        puller.continuous = true
        puller.channels = ["timeline"]
        var auth: CBLAuthenticatorProtocol?
        auth = CBLAuthenticator.basicAuthenticator(withName: "appcn", password: "#q1w2e3#")
        puller.authenticator = auth
        return puller
    }
    
    func closeConnection(){
        if(postDao != nil){
            postDao.closeConnection()
            postDao = nil
        }
    }
}
