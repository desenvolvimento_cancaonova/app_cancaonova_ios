//
//  FixedViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 18/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation

import UIKit
import Firebase
import Nuke
import StoreKit
import CouchbaseLite

class FixedViewController: UIViewController

{   
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblChannel: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
   // let syncGatewayUrl = URL(string: "http://dev.cancaonova.com:4985/fixed/")!
    
    var puller: CBLReplication!
    var database: CBLDatabase!
    
    var postFixed:Post!
    
   
    override func viewDidAppear(_ animated: Bool) {
        startDataBase()
    }
    func startDataBase(){
       
        
        let manager = CBLManager.sharedInstance()
        database = try! manager.databaseNamed("fixed")
        
        retrieveFixed()
        
        let urlDAO: URLDAO = URLDAO.init()
        let url:URL = urlDAO.getURLFromUserDefinedSettings(urlName: "url")
        puller = database.createPullReplication(url)
        puller.continuous = true
        puller.channels = ["fixed"]
        var auth: CBLAuthenticatorProtocol?
        auth = CBLAuthenticator.basicAuthenticator(withName: "appcn", password: "#q1w2e3#")
        puller.authenticator = auth
        
        NotificationCenter.default.addObserver(self, selector: #selector(pullProgress(notification:)),
                                               name: NSNotification.Name.cblReplicationChange, object: puller)
        
        puller.start()
    }
    
    @objc func pullProgress(notification: NSNotification) {
        let replication = notification.object as! CBLReplication
        
        if(replication.status == .idle){
            self.retrieveFixed()
        }
    }
    func retrieveFixed(){
        let query =  try!  CBLQueryBuilder (database: database, select: ["id","title", "content","date","action", "type", "source", "image_url"],
                                          where: "1=1", orderBy: ["-date"])
        let queryObj = query.createQuery(withContext: nil)
        queryObj.limit = 1
        
        let result = try! queryObj.run()
        
        if( result.count > 0 ){
            
            let row:CBLQueryRow = result.row(at: 0)
            postFixed = Post(for: (row.document)!)!
            
            lblTitle.text = postFixed.title
            lblChannel.text = postFixed.source
            lblDescription.text = postFixed.content
    
            let image_url:String = postFixed.image_url.object(forKey: "thumbnail") as! String
            
            if(image_url != ""){
                let url:NSURL = NSURL(string:image_url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
                Nuke.loadImage(with: url as URL, into: imgThumb)
            }
            
            showHideFixed(show: true)
        }else{
            showHideFixed(show: false)
        }
     }
    
    
    func showHideFixed(show: Bool){
        let infoData:[String: Bool] = ["show": show]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fixedNotification"), object: nil, userInfo: infoData)
    }
    
    @IBAction func fixedAction(sender: UIGestureRecognizer) {
       let infoData:[String: Any] = ["post": postFixed, "indexPath": IndexPath(row: 0, section: 0)]
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "actionNotification"), object: nil, userInfo: infoData)
    }
    
    
}
