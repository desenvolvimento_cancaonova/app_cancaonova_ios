//
//  SuggestionViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 11/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit

class SuggestionViewController:UITableViewController {
    
    @IBOutlet weak var btSend: UIButton?
    @IBOutlet weak var txtSuggestion: UITextView?
    @IBOutlet weak var txtEmail: UITextField?
    var alert: UIAlertController!
    
    override func viewDidLoad() {
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        applyBorderToTextElements()
        retrieveFormValues()
    }
    
    @IBAction func sendSuggestion(sender: UIButton){
        if(txtSuggestion?.text != ""){
            sendMail()
        }else{
            let infoData:[String: Any] = ["message": " Por favor, insira uma sugestão!"]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: infoData )
        }
    }
    
    @IBAction func dismissKeyboard (){
        txtEmail?.resignFirstResponder()
        txtSuggestion?.resignFirstResponder()
    }
    
    
    //TODO centralizar o envido de email  pq é utlizado no Prayer tb
    
    func sendMail(){
        
        
        alert = UIAlertController(title: "Envio de Sugestão", message: "Enviando...", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10,y: 5,width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        
        DispatchQueue.main.async {
            self.present(self.alert, animated: true)
            
        }

        
        let email:String = txtEmail!.text!
        let text:String = txtSuggestion!.text!
        let urlDAO: URLDAO = URLDAO.init()
        
        let bodyData = "subject=Sugestão APP CN &email=\(email)&suggestion=\(text)"
        let session = URLSession.shared
        
        //let request = NSMutableURLRequest(url: NSURL(string:"https://www.cancaonova.com/app-email-redirect")! as URL)
        let request = NSMutableURLRequest(url: urlDAO.getURLFromCB(urlName: "url_send_mail"))
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
 
        request.httpBody = bodyData.data(using: String.Encoding.utf8)
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                self.alert.dismiss(animated: false, completion: {
                    let infoData:[String: Any] = ["message": "Não Foi possivel enviar sugestão, tente novamente"]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: infoData )
                })
                return
            }
       
            self.alert.dismiss(animated: false, completion: {
                self.txtSuggestion?.text = ""
                UserDefaults.standard.set(self.txtEmail?.text, forKey: "emailSugestao")
                let infoData:[String: Any] = ["message": "A sua sugestão foi enviada com sucesso!"]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: infoData )
            })
            
        }
        
        task.resume()
    }
    
    func applyBorderToTextElements(){
    
        txtSuggestion?.layer.borderColor = UIColor.lightGray.cgColor
        txtSuggestion?.layer.borderWidth = 0.5
       
        applyBottomLineToTextField(textField: txtEmail!)
       
    }
    
    func applyBottomLineToTextField(textField: UITextField){
        
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor(red: 0.67, green: 0.71, blue: 0.73, alpha: 1.0).cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  self.view.frame.size.width-32, height: textField.frame.size.height)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
    }
    
    func retrieveFormValues(){
        
        self.txtEmail?.text = UserDefaults.standard.string(forKey: "emailSugestao")
        
    }
}
