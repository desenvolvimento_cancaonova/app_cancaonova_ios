//
//  ReaderController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 28/06/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit




class ReaderController{
    //var readerdao:ReaderDAO
    
    //var delegate:StateDelegate!
    
   /* init(delegate:StateDelegate ){
        //self.delegate = delegate
        readerdao = ReaderDAO.init()
    }*/
    
    /**
     status = true salvo para ler depois
     status = false = removido do ler depois
     Não precisamos um tratamento de erro para a acao no banco de dados
     */
    
    func save(post:Post, indexPath:IndexPath, sender:Any){
        let readerdao = ReaderDAO.init()
        var status:Bool  = readerdao.isActived (model: post)
        
        if (status == false){
            status = readerdao.addReader(post: post)
        }else{
            status = readerdao.delete(model: post)
            if status == true {
                status = false
            }
        }
        
        let infoData:[String: Any] = ["indexPath": indexPath, "status": status]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReaderChangeNotification"), object: nil, userInfo: infoData)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuBadgeChange"), object: nil, userInfo: nil)
        
        readerdao.closeConnection()
        
        //delegate.changeState(state: status, sender: sender)
    }
  /*
    func prepareToDelete(post:Post, indexPath:IndexPath){
        readerdao.prepareToDelete(documentID: post.firstAction())
        
        let infoData:[String: Any] = ["indexPath": indexPath, "status": false]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReaderChangeNotification"), object: nil, userInfo: infoData)
        
    }
    
    func prepareToDeleteRemove(post:Post, indexPath:IndexPath){
        readerdao.prepareToDeleteRemove(documentID: post.firstAction())
        
        let infoData:[String: Any] = ["indexPath": indexPath, "status": true]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReaderChangeNotification"), object: nil, userInfo: infoData)
      
    }*/

 
    
}
