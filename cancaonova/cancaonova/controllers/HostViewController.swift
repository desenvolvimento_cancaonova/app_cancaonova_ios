 

import UIKit
import InteractiveSideMenu

class HostViewController: MenuContainerViewController {
   
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let navMenu = self.storyboard!.instantiateViewController(withIdentifier: "NavigationMenu") as! NavigationMenuViewController
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.0, green: 0.51, blue: 0.74, alpha: 1.0)
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        menuViewController = navMenu
        
        navMenu.loadFirst()
 
        addBarButton( )
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addBarButton), name: NSNotification.Name(rawValue: "menuBadgeChange"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.msgInteractionNotification(notification:)), name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil)
        
        let storyboardName: String = self.storyboard?.value(forKey: "name") as! String
        
        if storyboardName == "iPad" {
            let screenSize: CGRect = UIScreen.main.bounds
            self.transitionOptions = TransitionOptions(duration: 0.5, visibleContentWidth: screenSize.width/2)
        }
        
        
    }
    
   /* override func menuTransitionOptionsBuilder() -> TransitionOptions {
        
        return TransitionOptionsBuilder() { builder in
            builder.duration = 0.5
            builder.contentScale = 1
        }
    }*/
    
    
   override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    
        var options = TransitionOptions()
    
        options.duration = 0.5
        options.contentScale = 1
        self.transitionOptions = options
    }
    
 
    @objc func barButtonTapped(sender: UIBarButtonItem) {
        showSideMenu()
    }
    
    @objc func addBarButton( ){        
        
        let dao:ReaderDAO = ReaderDAO()
        let count:Int = dao.count()
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 50))
        rightButton.setImage(#imageLiteral(resourceName: "icon_menu_40x50"), for: .normal)
        rightButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
       
        if(count > 0 ){
            let label = UILabel(frame: CGRect(x: 35, y: 5, width: 15, height: 15))
            label.layer.cornerRadius = label.bounds.size.height / 2
            label.layer.masksToBounds = true
            label.text = ""
            label.backgroundColor = UIColor(red: 0.97, green: 0.34, blue: 0.44, alpha: 1)
            rightButton.addSubview(label)
        }
        
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        
        navigationItem.leftBarButtonItem = rightBarButtomItem
    }
    
    @objc func msgInteractionNotification(notification: NSNotification){
        if let message = notification.userInfo?["message"] as? String {
            msgInteration(message: message)
        }
    }
    
    func msgInteration(message: String){
        let alertController = UIAlertController(title: "", message:message, preferredStyle: .actionSheet)
        
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: 177, y: 947, width: 500, height: 57)
        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        
        DispatchQueue.main.async{
            self.present(alertController, animated: true, completion: {
                let when = DispatchTime.now() + 3
                
                DispatchQueue.main.asyncAfter(deadline: when) {
                    alertController.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    

}
