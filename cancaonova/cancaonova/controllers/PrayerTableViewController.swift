//
//  PrayerViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 30/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import InteractiveSideMenu
import UIKit

class PrayerTableViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate{
 
    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPedido: UITextView!
    var activeFrame: CGRect?
    var alert: UIAlertController!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = "Pedir Oração"
        
        retrieveFormValues()
        applyBorderToTextElements()
    }
   
 

    @IBAction func viewTap(sender:UIGestureRecognizer){
        self.txtNome.resignFirstResponder()
        self.txtEmail.resignFirstResponder()
        self.txtPedido.resignFirstResponder()
        
    }
    
    @IBAction func btSend(sender:UIButton){

         if txtPedido.text.characters.count > 0 {
            sendPrayer()
         }else{
            let infoData:[String: Any] = ["message": " Por favor, insira o pedido da Oração."]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: infoData )
        }
    }
    
    func sendPrayer(){
        
        alert = UIAlertController(title: "Pedido de Oração", message: "Enviando...", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10,y: 5,width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        
        DispatchQueue.main.async {
            self.present(self.alert, animated: true, completion: nil)
        }
        
        
        let name:String = txtNome.text!
        let email:String = txtEmail.text!
        let text:String = txtPedido.text!
        let urlDAO: URLDAO = URLDAO.init()
        let sendPrayerEmail:String = urlDAO.getURLFromCB(urlName: "prayer_to").path
        let bodyData = "to=\(sendPrayerEmail)&subject=Pedido de Oração&username=\(name)&email=\(email)&suggestion=\(text)"
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: urlDAO.getURLFromCB(urlName: "url_send_mail"))
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        request.httpBody = bodyData.data(using: String.Encoding.utf8)
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                
                self.alert.dismiss(animated: false, completion: {
                    let infoData:[String: Any] = ["message": "Não Foi possivel enviar o seu pedido, tente novamente."]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: infoData)
                })
                
                return
            }
            DispatchQueue.main.async {
               self.alert.dismiss(animated: false, completion: {
                    let infoData:[String: Any] = ["message":  "O seu Pedido de Oração foi enviado com sucesso!!"]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: infoData )
                    UserDefaults.standard.set(self.txtNome.text, forKey: "nomePedido")
                    UserDefaults.standard.set(self.txtEmail.text, forKey: "emailPedido")
                    self.txtPedido.text = ""
                })
            }
        }
        
        task.resume()
        
        urlDAO.closeConnection()
    }
    
    func applyBorderToTextElements(){
        
        txtPedido?.layer.borderColor = UIColor.lightGray.cgColor
        txtPedido?.layer.borderWidth = 0.5
        
        applyBottomLineToTextField(textField: txtNome)
        applyBottomLineToTextField(textField: txtEmail)
        
    }
    
    func applyBottomLineToTextField(textField: UITextField){
    
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor(red: 0.67, green: 0.71, blue: 0.73, alpha: 1.0).cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
    }
    
    func retrieveFormValues(){
    
        self.txtEmail.text = UserDefaults.standard.string(forKey: "emailPedido")
        self.txtNome.text = UserDefaults.standard.string(forKey: "nomePedido")
    
    }

}
