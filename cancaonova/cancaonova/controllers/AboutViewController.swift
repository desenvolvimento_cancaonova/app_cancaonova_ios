//
//  AboutViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 11/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit
import InteractiveSideMenu
import StoreKit

class AboutViewController: UIViewController, SideMenuItemContent, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelDescription: UILabel!
    
    
    let labels = ["Avalie o App", "Sugestões", "Termos de Uso ", "Nossos Aplicativos", "Notificações"]
    let notification_description:String = "Desative essa opção para não receber mais notificações"
    
    
    override func viewDidLoad() {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        labelDescription.text = "Aplicativo Canção Nova - Versão \(version ?? "1.0.0") Copyright Canção Nova"
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        navigationController?.navigationBar.topItem?.title = "Sobre"
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell!
        var fontSize:CGFloat = 17
    
        if(indexPath.row == 4){
            let notificationCell = tableView.dequeueReusableCell(withIdentifier: "AboutNotificationCell", for: indexPath) as! AboutNotificationCell
            var notificationDescriptionFontSize:CGFloat = 8
            
            if (UI_USER_INTERFACE_IDIOM() == .pad){
                fontSize = 20
                notificationDescriptionFontSize = 12
            }
            
            notificationCell.lblTitle?.font = notificationCell.textLabel?.font.withSize(fontSize)
            notificationCell.lblTitle?.text = labels[indexPath.row]
            
            notificationCell.lblDetail?.text = notification_description
            notificationCell.lblDetail?.font = notificationCell.textLabel?.font.withSize(notificationDescriptionFontSize)
            notificationCell.separatorInset = UIEdgeInsetsMake(0.0 , notificationCell.bounds.size.width , 0.0, -notificationCell.bounds.size.width)
            
            cell = notificationCell
            
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell", for: indexPath)
            cell.textLabel?.text = labels[indexPath.row]
            
            if (UI_USER_INTERFACE_IDIOM() == .pad){
                fontSize = 20
            }
            cell.textLabel?.font = cell.textLabel?.font.withSize(fontSize)
            cell.textLabel?.textColor = UIColor(red: 0.40, green: 0.45, blue: 0.49, alpha: 1);
        }
            
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 0:
            
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                let url = NSURL(string:"https://itunes.apple.com/app/id1233394997" )
                UIApplication.shared.openURL(url! as URL)
            }
        case 1:
        
            let vc: SuggestionViewController = storyboard!.instantiateViewController(withIdentifier: "SuggestionViewController") as! SuggestionViewController
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2:
            
            let vc: TermsViewController = storyboard!.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
            self.navigationController?.pushViewController(vc, animated: true)

            break
        case 3:
            
            let url = NSURL(string:"https://apps.apple.com/developer/cancao-nova/id414480087" )
            UIApplication.shared.openURL(url! as URL)
            
            break
        default:
            break
        }
    }
    
}
