//
//  LikeViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 05/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit
import CouchbaseLite

class LikeViewController: ContentViewController, UIGestureRecognizerDelegate{
    var blurEffect:UIBlurEffect!
 
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationController?.navigationBar.topItem?.title = "Itens que eu gostei"
        
        if self.postList.count == 0 {
            self.postList = [Post]()
            retrieveTimeline()
        }
        
          //NotificationCenter.default.addObserver(self, selector: #selector(self.LikeChangeNotification(notification:)), name: NSNotification.Name(rawValue: "LikeChangeNotification"), object: nil)
    }
    
    
    internal override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let post:Post = postList[indexPath.row]
        let cell:PostViewCell
        
       if(post.widget == "loja"){
          cell = createWidgetShopping(tableView: tableView, cellForRowAt: indexPath, post: post)
       }else
        if(post.widget == "doacao"){
            cell = createWidgetDonation(tableView: tableView, cellForRowAt: indexPath, post: post)
        }else{
            cell = createWidgetDefault(tableView: tableView, cellForRowAt: indexPath, post: post)
        }
        let border = CALayer()
        
        border.borderColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0).cgColor
        border.frame = CGRect(x: 10, y: 10, width:  cell.frame.size.width-20, height: cell.frame.size.height-20)
        
        border.borderWidth = 1
        cell.layer.addSublayer(border)
        cell.layer.masksToBounds = true
 
        cell.indexPath = indexPath
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }

    
     override  func retrieveTimeline(){
        lastKey = nil
        postList = [Post]()
        loadResult = true
        let dao:LikeDAO = LikeDAO.init()
        let result = dao.select(lastKey: self.lastKey)
        self.receivePost(result: result)
        
        dao.closeConnection()
        
        if  (result.count == 0){
            self.hideLoadingWidget()
        }
        
    }
    
    //TODO esse metodo se repete no ReaderViewController
    override func populatePost(result:CBLQueryEnumerator ){
        
        for i in 0..<result.count{
            let row:CBLQueryRow = result.row(at: UInt(i))
            let post = pc.select(documentID: row.documentID!)
            self.postList.append(post)
            
            lastKey = row.key
            
        }
    }
    
    // MARK: Notification
    override func LikeChangeNotification(notification: NSNotification){
        
        
        if let indexPath = notification.userInfo?["indexPath"] as? IndexPath {
            
            DispatchQueue.main.async {
                self.postList .remove(at: indexPath.row)
                super.tableView.reloadData()
                if(self.postList.count <= 0){
                    self.retrieveTimeline()
                }
            }
            
            
        }
        
    }
   
}
