
import Foundation
import UIKit

class LikeController{
    var post:Post!
    var lblLike:UILabel?
    var statusAction:Bool!

    var indexPath:IndexPath!
    var button:Any!
    var task:URLSessionDataTask!
    
    init( post:Post, lblLike: UILabel){
        self.post = post
        self.lblLike = lblLike
    }
    
    init( post:Post ){
        self.post = post
    }

    func isActived()->Bool{
        let likedao:LikeDAO = LikeDAO.init( )
        self.checkLikes()
        
        let retorno = likedao.isActived (model: post)
        
        likedao.closeConnection()
        
        return retorno
    }
    
    func sendLike(type:String, indexPath:IndexPath, sender:Any){
        
        self.button =  sender
      
        self.indexPath = indexPath
      
        let urlDAO: URLDAO = URLDAO.init()
        let request = NSMutableURLRequest(url: urlDAO.getURLFromCB(urlName: "url_send_like"))
        
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let paramString = "link=\(self.post.link)&date=\(NSDate())&type=\(type)&origin=appcn-ios&id=\(self.post.id)"
        request.httpBody = paramString.data(using: String.Encoding.utf8)
 
        
       task =  URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                
                self.msgErroLike()
                return
            }
        
            do{
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions .mutableContainers) as? [String:Any]{
                    
                    if (json["success"] as! Bool){
                        self.actionLikes(type: type)
                        
                    }else{
                        self.msgErroLike()
                    }
                }
            }catch{
                    self.msgErroLike()
            }
        }
        
        task.resume()
      
    
    }
    
    func msgErroLike(){
 
        let infoData:[String: Any] = ["indexPath": self.indexPath, "message": "Desculpe, tivemos problemas em executar a ação. Verifique sua conexão ou tente mais tarde!"]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LikeChangeNotification"), object: nil, userInfo: infoData)

    }
    
    func actionLikes(type:String){
       
            let likedao:LikeDAO = LikeDAO.init()
            if(type=="like"){
                self.statusAction = likedao.addLike(post: self.post)
                self.post.addLike(like: 1)
            }else{
                self.statusAction = likedao.delete(model: self.post)
                self.post.removeLike(like: 1)
            }
        
        task.cancel()
        
        likedao.closeConnection()
        
        checkLikes();
        
        let infoData:[String: Any] = ["indexPath": self.indexPath]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LikeChangeNotification"), object: nil, userInfo: infoData)
 
    }
    
    func checkLikes(){
        if(self.lblLike != nil){
            DispatchQueue.main.async {
                if(!(self.lblLike?.text?.isEmpty)!){
                    DispatchQueue.main.async(execute: {
                        let likes = self.post.likes()
                        if  likes > 0 {
                            var likeText:String = "gostaram"
                            if(likes == 1){
                                likeText = "gostou"
                            }
                            let likesWithDecimalSeparators = NumberFormatter.localizedString(from: NSNumber(value: likes), number: NumberFormatter.Style.decimal)
                            self.lblLike?.text = "\(likesWithDecimalSeparators) \(likeText)"
                            self.lblLike?.isHidden = false
                            
                        }else{
                            self.lblLike?.isHidden = true
                        }
                    })
                }
            }
            
        }
    }
    
    
    
}
