//
//  ContentViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 24/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import Nuke
import StoreKit
import CouchbaseLite
import InteractiveSideMenu


class ContentViewController: UIViewController, SideMenuItemContent, UITableViewDelegate, UITableViewDataSource, SKStoreProductViewControllerDelegate{
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading:   UIActivityIndicatorView!
    @IBOutlet weak var viewWithout: UIView!
    
    var postList = [Post]()
    var loadResult: Bool = false
    var lastKey:Any!
    var currentCountItem:Int = 0
    
    let pc = PostController.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.bounces = false
        
        self.loading.startAnimating()
        self.loading.isHidden = false
        self.loading.alpha = 1
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.shareNotification(notification:)), name: NSNotification.Name(rawValue: "shareNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionNotification(notification:)), name: NSNotification.Name(rawValue: "actionNotification"), object: nil)
     
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReaderChangeNotification(notification:)), name: NSNotification.Name(rawValue: "ReaderChangeNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.LikeChangeNotification(notification:)), name: NSNotification.Name(rawValue: "LikeChangeNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTableView), name: NSNotification.Name(rawValue:"UpdateTableView"), object: nil)
    
    }
    
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postList.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let post:Post = postList[indexPath.row]
        let cell:PostViewCell
        
        if(post.widget == "loja"){
            cell = createWidgetShopping(tableView: tableView, cellForRowAt: indexPath, post: post)
        }else 
        if(post.widget == "doacao"){
            cell = createWidgetDonation(tableView: tableView, cellForRowAt: indexPath, post: post)
        }else{
            cell = createWidgetDefault(tableView: tableView, cellForRowAt: indexPath, post: post)
        }
        
        cell.indexPath = indexPath
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cel:PostViewCell = cell as! PostViewCell
        
        cel.btLike.isHighlighted = false
        cel.btShare.isHighlighted = false
        
        if let bt:UIButton = cel.btReader  {
            bt.isHighlighted = false
        }
        
//        if(loadResult == false){
//            if(postList.count > 99){
//                if (postList.count - indexPath.row  < 50) {
//                    retrieveTimeline()
//                }
//            }
//        }
    }
    
    
    
    func retrieveTimeline(){
        
        loadResult = true
        currentCountItem = pc.countPosts()
        let result = pc.select(lastKey: lastKey)
        self.receivePost(result: result)
        
    }
    
    
    func receivePost(result:CBLQueryEnumerator ){
        
        if(result.count > 0 ){
            
            hideLoadingWidget()
            tableView.isHidden = false
            if(viewWithout != nil){
                viewWithout.isHidden = true
            }
            
            populatePost(result: result)
            
            finishLoad()
            
        }else{
            withoutPost()
            //showLoadingWidget()
        }
        
        loadResult = false
        
    }
    
    
    func populatePost(result:CBLQueryEnumerator ){
        for i in 0..<result.count{
            let row:CBLQueryRow = result.row(at: UInt(i))
            
            let post:Post = Post(for: (row.document)!)!
             self.postList.append(post)
            
            lastKey = row.key
            
        }

    }
    
    func finishLoad(){
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
            /*UIView.animate(withDuration: 0.5, animations: {  () -> Void in
               // self.loading.alpha = 0
            })*/
        })
        
    }
    func withoutPost(){
        showLoadingWidget()
        finishLoad()
        if(viewWithout != nil){
            viewWithout.isHidden = false
            tableView.isHidden = true
        }
    }
    
    
    // MARK: - WidgetViews
    
    func createWidgetShopping( tableView: UITableView, cellForRowAt indexPath: IndexPath , post:Post)->PostViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellShopping", for: indexPath) as! PostViewCell
        
        cell.lblTitle.text = post.title
        cell.lblChannel.text = post.source
        
        cell.setPost(post: post)
        
        let image_url:String = post.image_url.object(forKey: "thumbnail") as! String
        cell.imgThumb.image = nil
        if(image_url != ""){
            let url:NSURL = NSURL(string:image_url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
            Nuke.loadImage(with: url as URL, into: cell.imgThumb)
        }
        
        return cell;
        
    }
    
    
    func createWidgetDonation( tableView: UITableView, cellForRowAt indexPath: IndexPath , post:Post)->PostViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellDonation", for: indexPath) as! PostViewCell
        cell.setPost(post: post)
        
        cell.lblTitle.text = post.title
        cell.lblDescription.text = "\(post.content)%"
       // cell.cpDonation.value = CGFloat((post.content as NSString).floatValue)
        
        //let date = NSDate(timeIntervalSince1970: post.date )
        //let time:String = NSDate().timeOfOccurrence(date: Date() as NSDate, andDate: date  )
        
       // cell.lblTime.text = "atualizado \(time.lowercased())"
        
        return cell;
        
    }
    
    func createWidgetDefault( tableView: UITableView, cellForRowAt indexPath: IndexPath , post:Post)->PostViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPost", for: indexPath) as! PostViewCell
        cell.lblChannel.text = post.source
        cell.lblTitle.text = post.title
        cell.lblDescription.text = post.content
        cell.lblDescription.textAlignment = .natural
        cell.lblDescription.frame.size.width = 533
        cell.lblDescription .numberOfLines = 3
        cell.lblDescription .sizeToFit()
        //cell.imgThumb.layer.borderWidth = 0.5
        //cell.imgThumb.layer.borderColor = UIColor(red: 0.67, green: 0.71, blue: 0.73, alpha: 1.0).cgColor
        self.applyOutline(postViewCell: cell)
        cell.setPost(post: post)
        
        let date = NSDate(timeIntervalSince1970: post.date )
        
        let time:String = NSDate().timeOfOccurrence(date: Date() as NSDate, andDate: date  )
        cell.lblTime.text = time
        
        if let image_url:String = post.image_url.object(forKey: "thumbnail") as? String {
            cell.imgThumb.image = nil
            
            if(image_url != ""){
                let url:NSURL = NSURL(string:image_url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
                Nuke.loadImage(with: url as URL, into: cell.imgThumb)
            }
        }
        return cell;
        
    }
    
    
    func applyOutline(postViewCell: PostViewCell){
        
        postViewCell.imgThumb.layer.borderWidth = 0.5
        postViewCell.imgThumb.layer.borderColor = UIColor(red: 0.67, green: 0.71, blue: 0.73, alpha: 1.0).cgColor
        
    }
    
    
    func actions(post:Post, indexPath:IndexPath){
        
        if(post.widget == "post"){
            let vc: WebContentViewController = storyboard!.instantiateViewController(withIdentifier: "WebContentViewController") as! WebContentViewController
            
            vc.setPost(post: post, indexPath: indexPath)
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else if(post.widget == "loja" || post.widget == "doacao" || post.widget == "url_externa" ){
            
            
            let url:NSURL = NSURL(string: appInUrl(url: post.firstAction())   )!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
            
            
            
        }else if(post.widget == "app" ){
            
            let appUrl = NSURL(string:"\(post.firstAction())://" )
            
            if UIApplication.shared.canOpenURL(appUrl! as URL){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appUrl! as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(appUrl! as URL)
                }
                
            } else {
                let action: String =  post.firstAction()
                let idApple = action.replacingOccurrences(of: "cn", with: "")
                let vc: SKStoreProductViewController = SKStoreProductViewController()
                
                let params = [
                    SKStoreProductParameterITunesItemIdentifier:idApple,
                    ]
                
                vc.delegate = self
                vc.loadProduct(withParameters: params, completionBlock: nil)
                self.present(vc, animated: true) { () -> Void in }
            }
        }
        
    }
    
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func appInUrl( url:String) ->String {
        let retorno:String
        
        if ( url.range(of: "?") != nil) {
            retorno = "\(url)&app=cn&readMode=true";
        } else {
            retorno = "\(url)?app=cn&readMode=true";
        }
        return retorno;
    }
    
    
    // MARK: Notifications
    
    @objc func actionNotification(notification: NSNotification){
        if let post = notification.userInfo?["post"] as? Post {
            let indexPath : IndexPath = notification.userInfo?["indexPath"] as! IndexPath
            actions(post: post, indexPath: indexPath)
        }
    }
    
    
    @objc func shareNotification(notification: NSNotification){
        
        if let post = notification.userInfo?["post"] as? Post {
            
            var objectsToShare = [AnyObject]()
            objectsToShare.append(post.titleToShare() as AnyObject)
            objectsToShare.append(NSURL(string: post.actionToShare())!)
            
            if let image = notification.userInfo?["image"] as? UIImage {
                objectsToShare.append(image)
            }
            
            let alert = UIAlertController(title: nil, message: "Aguarde...", preferredStyle: .alert)
            
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
            let queue = DispatchQueue(label: "openActivityIndicatorQueue")
            
            queue.async(execute: {() -> Void in
                
                
                
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivityType.postToWeibo, UIActivityType.assignToContact, UIActivityType.print, UIActivityType.copyToPasteboard, UIActivityType.saveToCameraRoll]
                
                DispatchQueue.main.async(execute: {() -> Void in
                    alert.dismiss(animated: true, completion:{
                        
                        if (UI_USER_INTERFACE_IDIOM() == .pad){
                            activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                            activityVC.popoverPresentationController?.sourceView = self.view;
                            activityVC.popoverPresentationController?.sourceRect = CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: 0, height: 0)
                        }
                        self.present(activityVC, animated: false,completion:nil)
                        
                    })
                })
                
                
            })
            
        }
    }
 
    
 
    
    @objc func ReaderChangeNotification(notification: NSNotification){
        
        if let indexPath = notification.userInfo?["indexPath"] as? IndexPath {
            let cell:PostViewCell = tableView.cellForRow(at: indexPath) as! PostViewCell
            cell.checkReaderButtonState()
        }
    }
    
    @objc func LikeChangeNotification(notification: NSNotification){
        
        if let indexPath = notification.userInfo?["indexPath"] as? IndexPath {
            
            DispatchQueue.main.async {
                if let cell:PostViewCell = self.tableView.cellForRow(at: indexPath) as? PostViewCell {
                    cell.checkLikeButtonState()
                }
            }
            
        }
        
        if (notification.userInfo?["message"] as? String) != nil {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: notification.userInfo )
        }
    }
    
    
    func showLoadingWidget(){
        
        loading.isHidden = false
        loading.startAnimating()
    
    }
    
    func hideLoadingWidget(){
    
        loading.isHidden = true
        loading.stopAnimating()
    }
    
    @objc func updateTableView(){
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }

    }
    
}
