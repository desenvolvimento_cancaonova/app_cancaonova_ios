 
import UIKit
import InteractiveSideMenu
import StoreKit
import UICircularProgressRing
import CouchbaseLite

class NavigationMenuViewController: MenuViewController, UITableViewDelegate,UITableViewDataSource {
    
    var badge: UIImageView!
    var labelBadge:UILabel!
    var timer:Timer = Timer()
    var monthPercent = ""
    var valuePercent:CGFloat = 0
    var postDonate:Post!
    var selectedRow:Int = 0
    let selectedColor:UIColor = UIColor.white
    let normalColor:UIColor   = UIColor(red:1.0, green:1.0, blue:1.0, alpha:0.7)
    var keepSelection:Bool = false
    
    
    let labels = ["Meu Feed (Início)", "Gostei", "Ler Depois", "Pedir Oração", "Onde Estamos", "Loja Canção Nova", "Sobre"]
    let icons = [#imageLiteral(resourceName: "ic_menu_feed"), #imageLiteral(resourceName: "ic_menu_like"), #imageLiteral(resourceName: "ic_menu_reader"), #imageLiteral(resourceName: "ic_menu_prayer"), #imageLiteral(resourceName: "ic_menu_marker"), #imageLiteral(resourceName: "ic_menu_shopping"), #imageLiteral(resourceName: "ic_menu_about")]
    let icons_over = [#imageLiteral(resourceName: "ic_menu_feed_over"), #imageLiteral(resourceName: "ic_menu_like_over"), #imageLiteral(resourceName: "ic_menu_reader_over"), #imageLiteral(resourceName: "ic_menu_prayer_over"), #imageLiteral(resourceName: "ic_menu_marker_over"), #imageLiteral(resourceName: "ic_menu_shopping_over"), #imageLiteral(resourceName: "ic_menu_about_over")]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgPercent: UIImageView!
    @IBOutlet weak var lblPercent: UILabel!
    @IBOutlet weak var cpDonation: UICircularProgressRingView!
    @IBOutlet var donationView: UIView!
    
 
    var puller: CBLReplication!
    var database: CBLDatabase!
    
    @objc func pullProgress(notification: NSNotification) {
        let replication = notification.object as! CBLReplication
        
        if(replication.status == .idle){
            replicationProgress()
        }
        
        if(replication.status == .offline){
            replicationProgress()
        }

    }
  
    
    func openConnection(){
        let manager = CBLManager.sharedInstance()
        database = try! manager.databaseNamed("config")
 
    }
    
    func replicationProgress(){
        
        openConnection()
        
        let query =  try!  CBLQueryBuilder (database: database, select: ["id", "title", "content" ],  where: "source_slug='campanha'",orderBy: ["-date"])
        
        let queryObj = query.createQuery(withContext: nil)
        queryObj.limit = 1
        
        let result = try! queryObj.run()
        
        if( result.count > 0 ){
            
            let row:CBLQueryRow = result.row(at: 0)
            postDonate = Post(for: (row.document)!)!
            
            valuePercent = CGFloat((postDonate.content as NSString).floatValue)

            let index =  postDonate.title.index( postDonate.title.startIndex, offsetBy: 3)
            monthPercent =  postDonate.title.substring(to: index).uppercased()
 
        }

    }
    override func viewDidLoad() {
       super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
  
        let configDAO: ConfigDAO = ConfigDAO.init()
        configDAO.startPuller()
        NotificationCenter.default.addObserver(self, selector: #selector(pullProgress(notification:)),
                                               name: NSNotification.Name.cblReplicationChange, object: puller)
        let screenSize: CGRect = UIScreen.main.bounds
        super.menuContainerViewController?.transitionOptions.visibleContentWidth = (screenSize.width*20)/100
        
    }
    override func viewWillAppear(_ animated: Bool) {

        if(badge != nil){
            addBadge(img: badge)
        }
        
        
        self.lblPercent.text = "\(Int(self.valuePercent))%"
        
        showDonation()
        createTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func loadFirst(){
        
        self.replicationProgress()
        
        self.menuContainerViewController?.selectContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "Timeline") as! SideMenuItemContent as! UIViewController)
    }
    
    func addBadge(img: UIImageView){
        let dao:ReaderDAO = ReaderDAO()
        let count:Int = dao.count()
        
        if(labelBadge != nil){
            labelBadge.removeFromSuperview()
        }
        if (count > 0){
            labelBadge = UILabel(frame: CGRect(x: 28, y: -5, width: 20, height: 20))
            labelBadge.clipsToBounds = true
            
            labelBadge.backgroundColor = UIColor(red: 0.97, green: 0.34, blue: 0.44, alpha: 1)
            labelBadge.textColor = UIColor.white
            labelBadge.text = "\(count)"
            labelBadge.font = labelBadge.font.withSize(10)
            labelBadge.textAlignment = NSTextAlignment.center
            
            labelBadge.layer.cornerRadius = 10
            
            img.addSubview(labelBadge)
        }
        
         badge = img
        
    }
    
    func selectFirst(label:UILabel){
        label.textColor = selectedColor
        self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.top)
    }
    
    func showDonation(){
        if(self.valuePercent > 0){
            //self.donationView.isHidden = false
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when) {
               // self.cpDonation.setProgress(value: self.valuePercent, animationDuration: 1)
            }
        }
    }
 
    @objc func animation(){
        UIView.transition(with: imgPercent!, duration: 0.5, options:.transitionFlipFromRight, animations: { () -> Void in
            
            if(self.valuePercent > 0){
                self.lblPercent.isHidden = true
                if(self.lblPercent.text == self.monthPercent){
                    self.lblPercent.text = "\(Int(self.valuePercent))%"
                    self.imgPercent.image = #imageLiteral(resourceName: "fundo_status_porcentagem")
                }else{
                    self.lblPercent.text = self.monthPercent
                    self.imgPercent.image = #imageLiteral(resourceName: "fundo_status_mes")
                }
                /*if(self.valuePercent != self.cpDonation.currentValue){
                    self.showDonation()
                }*/
            }
            
            
        }, completion: { (Bool) -> Void in
            
            self.view.layoutIfNeeded()
            self.lblPercent.isHidden = false
            
            self.createTimer()
        })
    }
    
    func createTimer(){
        timer.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.animation), userInfo: nil, repeats: false)
    }
    
    
    @IBAction func donateAction(sender: AnyObject) {
        
        let urlDAO: URLDAO = URLDAO.init()
        let url:URL = urlDAO.getURLFromCB(urlName: "url_donate")
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        closeMenu()
        urlDAO.closeConnection()
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.lblTitle?.text = labels[indexPath.row]
        
        if(selectedRow == indexPath.row){
            cell.lblTitle.textColor = selectedColor
            cell.imgThumb?.image = icons_over[indexPath.row]
        }else{
            cell.lblTitle.textColor = normalColor
            cell.imgThumb?.image = icons[indexPath.row]
        }
        
        if self.tableView.indexPathForSelectedRow == nil {
             self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.top)
        }
        
        if(indexPath.row == 2){
            addBadge(img: cell.imgThumb)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell:MenuCell = tableView.cellForRow(at: indexPath as IndexPath)! as! MenuCell
        selectedCell.imgThumb.image = icons_over[indexPath.row]
        
        if keepSelection == false {
            DispatchQueue.main.async {
                
               // guard let menuContainerViewController = self.menuContainerViewController else { return }
                var menuItemContentViewController:SideMenuItemContent!
                
                switch indexPath.row {
                    
                case 0:
                    
                    menuItemContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "Timeline") as! SideMenuItemContent
                case 1:
                    menuItemContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "Like") as! SideMenuItemContent
                case 2:
                    menuItemContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "Reader") as! SideMenuItemContent
                case 3:
                    menuItemContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "Prayer") as! SideMenuItemContent
                case 4:
                    let vc: WebContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebContentViewController") as! WebContentViewController
                    
                    let doc = self.database.createDocument()
                    let post = Post(for: (doc))!
                    
                    let nsdicAction:NSDictionary = ["ios" : "https://comunidade.cancaonova.com/onde-estamos?app=cn&readMode=true"]
                    let nsarAction:NSArray = [nsdicAction]
                    post.action = nsarAction
                    
                    let nsdicImage:NSDictionary = ["thumbnail" : ""]
                    //let nsarImage:NSArray = [nsdicImage]
                    post.image_url = nsdicImage
                    
                    vc.setPost(post: post, indexPath: indexPath)
                    
                    vc.setTitle(title: "Onde Estamos?")
                    
                    menuItemContentViewController = vc as! SideMenuItemContent
                case 5:
                    
                    let urlDAO: URLDAO = URLDAO.init()
                    let url:URL = urlDAO.getURLFromCB(urlName: "url_shopping")
         
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url as URL)
                    }
                    
                case 6:
                    menuItemContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "About") as! SideMenuItemContent
                default:
                    break
                }
                
                
                if(menuItemContentViewController != nil){
                    self.menuContainerViewController?.selectContentViewController(menuItemContentViewController as! UIViewController)
                    self.menuContainerViewController?.hideSideMenu()
                }else{
                    self.closeMenu()
                }
                
                
            }
        } else {
            keepSelection = false
        }
        
        
        if (indexPath.row != 5){
            selectedCell.lblTitle.textColor = selectedColor
            selectedRow = indexPath.row
        } else {
        
            self.tableView.selectRow(at: IndexPath(row: selectedRow, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
            if (self.tableView.indexPathsForVisibleRows?.contains(IndexPath(row: selectedRow, section: 0)))!{
                keepSelection = true
                self.tableView.delegate?.tableView!( self.tableView, didSelectRowAt: IndexPath(row: selectedRow, section: 0))
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
   
        if (self.tableView.indexPathsForVisibleRows?.contains(indexPath))!{
            if let cellToDeSelect:MenuCell = self.tableView.cellForRow(at: indexPath as IndexPath)! as? MenuCell {
                cellToDeSelect.lblTitle?.textColor = normalColor
                cellToDeSelect.imgThumb.image = icons[indexPath.row]
            }
        }
      
    }
    
    func closeMenu(){
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.menuContainerViewController?.hideSideMenu()
        }
    }
}
