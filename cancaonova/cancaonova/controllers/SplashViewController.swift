//
//  SplashViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 08/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
 
class SplashViewController: UIViewController{
    
    let configDAO: ConfigDAO = ConfigDAO.init()
    
    override func viewWillAppear(_ animated: Bool) {
        configDAO.startPuller()
    }
    
    override func  viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        playVideo()
    }
    
    
    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "splash", ofType:"mp4") else {
            return
        }
        
        let player = AVPlayer(url: NSURL(fileURLWithPath: path) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        self.view.layer.addSublayer(playerLayer)
        
        player.seek(to: kCMTimeZero)
        player.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appEnterBackground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)

    }
    
    
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        
        goToHostViewController()
    }
    
    
    @objc func appEnterBackground(note: NSNotification){
        //exit(0)
        goToHostViewController()
    }
    
    func goToHostViewController(){
    
        let vc : HostViewController = storyboard!.instantiateViewController(withIdentifier: "HostView") as! HostViewController
        
        let navigationController = UINavigationController(rootViewController: vc)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = navigationController
    }
    
}
