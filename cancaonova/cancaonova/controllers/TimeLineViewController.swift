
import UIKit
import CouchbaseLite

class TimeLineViewController: ContentViewController, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btUpdates: UIButton!
    
    
    var puller: CBLReplication!
    var showFixed:Bool = false
    var hasNews: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = "Canção Nova"
        NotificationCenter.default.addObserver(self, selector: #selector(self.fixedNotification(notification:)), name: NSNotification.Name(rawValue: "fixedNotification"), object: nil)
        
        startPuller()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if (puller != nil){
            puller.stop()
            puller = nil
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startPuller()
    }
    
    @objc func pullProgress(notification: NSNotification) {
        
        let replication = notification.object as! CBLReplication
        let status = replication.status
     
        switch status {
        case .idle:
            if(postList.count == 0){
                lastKey =  nil
                retrieveTimeline()
            }else if(hasNews){
                if(self.currentCountItem != pc.countPosts()){
                    showbtUpdate(alpha: 1, hidden: true)
                }
            }
        case .active:
            if(postList.count > 0 && replication.changesCount > 0){
                hasNews = true
            }
            
        case .offline:
            if(postList.count == 0){
                lastKey =  nil
                retrieveTimeline()
            }
        case .stopped:
            break
        }
    }
    
    func startPuller(){
        
        if(puller == nil){
            puller = pc.postDao.database.createPullReplication(startSyncData())
          
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.cblReplicationChange, object: puller)
            NotificationCenter.default.addObserver(self, selector: #selector(pullProgress(notification:)),
                                                   name: NSNotification.Name.cblReplicationChange, object: puller)
        }
       puller.continuous = true
        puller.channels = ["timeline"]
        var auth: CBLAuthenticatorProtocol?
        auth = CBLAuthenticator.basicAuthenticator(withName: "appcn", password: "#q1w2e3#")
        puller.authenticator = auth
        puller.start()
        
    }
    
    
    // MARK: User Actions
    
    @IBAction func btActionUpdates(sender: AnyObject){
        
        self.loading.startAnimating()
        self.loading.isHidden = false
        self.loading.alpha = 1
        
        self.showbtUpdate(alpha: 0, hidden: true)
        
        DispatchQueue.main.async(execute: {
            let top = IndexPath(row: 0, section: 0)
            self.postList = [Post]()
            self.lastKey = nil
            self.hasNews = false
            self.retrieveTimeline()
            self.tableView.scrollToRow(at: top, at: UITableViewScrollPosition.top, animated: true)
        })
    }
    
    @IBAction func donateAction(sender: AnyObject) {
        
        let urlDAO: URLDAO = URLDAO.init()
        let url:URL = urlDAO.getURLFromCB(urlName: "url_donate")
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        urlDAO.closeConnection()
    }
    
    func showbtUpdate (alpha: CGFloat, hidden:Bool){
        UIView.animate(withDuration: 0.5, animations: {  () -> Void in
            self.btUpdates.alpha = alpha
            
        })
    }
    
    func fixedHideShow(show: Bool){
        
        UIView.animate(withDuration: 0.25) { () -> Void in
            let firstView = self.stackView.arrangedSubviews[0]
            if(show == true){
                firstView.isHidden = false
            }else{
                firstView.isHidden = true
            }
        }
    }
    
    // MARK: Notification
    @objc func fixedNotification(notification: NSNotification){
        if let show = notification.userInfo?["show"] as? Bool {
            fixedHideShow(show: show)
        }
    }
    
    
    func startSyncData() -> URL{
        
        let urlDAO: URLDAO = URLDAO.init()
        let url:URL = urlDAO.getURLFromUserDefinedSettings(urlName: "url")
        
        return url
    }
    
    
}


