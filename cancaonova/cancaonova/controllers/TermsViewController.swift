//
//  TermsViewController.swift
//  cancaonova
//
//  Created by Desenvolvimento on 11/07/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import UIKit

class TermsViewController:UIViewController, UIWebViewDelegate  {
      @IBOutlet weak var webView: UIWebView!
      @IBOutlet weak var loading: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        self.loading.isHidden = false
        self.loading?.startAnimating()
        
        self.webView.scrollView.bounces = false
        self.webView.loadRequest(URLRequest(url: NSURL(string: "https://www.cancaonova.com/politica-de-privacidade/?readMode=true" )! as URL))
    }
    
    //MARK: WebViewDelegate
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loading?.stopAnimating();
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        let htmlString:String = "<br /><h2> Por favor, verifique sua conexão com a internet e tente novamente.</h2>"
        webView.loadHTMLString(htmlString, baseURL: nil)
    }

}
