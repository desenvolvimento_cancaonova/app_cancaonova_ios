
import Foundation

import UIKit
import Floaty
import Nuke
import InteractiveSideMenu

class WebContentViewController: UIViewController, UIWebViewDelegate, SideMenuItemContent{
   
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var menu:Floaty!
    
    var contents:String = ""
    var like:Bool   = false
    var reader:Bool = false
    var indexPath:IndexPath = [];
    var post: Post!
    var navTitle:String = ""
    
     let btShare: FloatyItem = FloatyItem()
     let btReader: FloatyItem = FloatyItem()
     let btLike: FloatyItem = FloatyItem()
   
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loading.isHidden = false
        self.loading?.startAnimating()
        self.webView.scrollView.bounces = false
 
        self.webView.loadRequest(URLRequest(url: NSURL(string: appInUrl(url: self.post.firstAction()))! as URL))
        
        self.checkStates()
        self.configItens()
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.LikeChangeNotification(notification:)), name: NSNotification.Name(rawValue: "LikeChangeNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReaderChangeNotification(notification:)), name: NSNotification.Name(rawValue: "ReaderChangeNotification"), object: nil)

        if(navTitle != ""){
            navigationController?.navigationBar.topItem?.title = navTitle
        }
    }
    
    
    func appInUrl( url:String) ->String {
        let retorno:String
        
        if ( url.range(of: "?") != nil) {
            retorno = "\(url)&app=cn&readMode=true";
        } else {
            retorno = "\(url)?app=cn&readMode=true";
        }

        return retorno;
    }
    
    func setPost(post: Post, indexPath:IndexPath){
        self.post = post
        self.indexPath = indexPath
    }
    
    func setTitle(title: String){
        self.navTitle = title
    }
    
    //MARK: States
    
    func checkStates(){
        self.checkReaderState()
        self.checkLikeState()
    }
    
    func checkReaderState(){
        let readerdao:ReaderDAO = ReaderDAO.init( )
        self.reader = readerdao.isActived (model: post)
        DispatchQueue.main.async {
            self.btReader.icon = self.reader ? #imageLiteral(resourceName: "ic_reader_over") : #imageLiteral(resourceName: "ic_reader")
        }
        readerdao.closeConnection()
    }
    
    func checkLikeState(){
        let likedao:LikeDAO = LikeDAO.init( )
        self.like = likedao.isActived (model: post)
        DispatchQueue.main.async {
            self.btLike.icon = self.like ? #imageLiteral(resourceName: "ic_like_over") : #imageLiteral(resourceName: "ic_like")
        }
        likedao.closeConnection()
    }
    
    //MARK: WebViewDelegate
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.loading?.stopAnimating();
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      
        let path = Bundle.main.path(forResource: "noconnection", ofType: "html")
        
        if(path != nil){
            let htmlURL: NSURL =   NSURL(fileURLWithPath: path!)
            webView.loadRequest(URLRequest(url: htmlURL as URL))
        }
        
    }

    
    //MARK: Action Menu
    
    func configItens(){
        
        btShare.icon = #imageLiteral(resourceName: "ic_share")
        btShare.handler = {
            item in
            self.share()
        }
        
        if(self.post.id == ""){
            menu.isHidden = true
        }
        
        menu.addItem(item: btShare)
    
        btReader.handler =  { item in
            self.saveReader(item: item)
        }
        menu.addItem(item: btReader)
        
        btLike.handler = { item in
            self.saveLike(item: item)
        }
        menu.addItem(item: btLike)

    }
    
    func share(){
        let image_url:String = self.post.image_url.object(forKey: "thumbnail") as! String
        var img:UIImage = #imageLiteral(resourceName: "ic_cn")
        
        if(image_url != ""){
            let request = Request(url:  NSURL(string:image_url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)! as URL)
            img = Cache.shared[request]!
            
        }
           let alert = UIAlertController(title: nil, message: "Aguarde...", preferredStyle: .alert)
            
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
            let queue = DispatchQueue(label: "openActivityIndicatorQueue")
            
            queue.async(execute: {() -> Void in
                
                var objectsToShare = [AnyObject]()
                objectsToShare.append(self.post.titleToShare() as AnyObject)
                objectsToShare.append(img)
                objectsToShare.append(NSURL(string: self.post.actionToShare())!)
                
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivityType.postToWeibo, UIActivityType.assignToContact, UIActivityType.print, UIActivityType.copyToPasteboard, UIActivityType.saveToCameraRoll]
                
                DispatchQueue.main.async(execute: {() -> Void in
                    alert.dismiss(animated: true, completion:{
                        
                        if (UI_USER_INTERFACE_IDIOM() == .pad){
                            activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                            activityVC.popoverPresentationController?.sourceView = self.view;
                            activityVC.popoverPresentationController?.sourceRect = CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: 0, height: 0)
                        }
                        self.present(activityVC, animated: false,completion:nil)
                        
                    })
                })
                
                
            })
     }
    
    func saveReader(item: FloatyItem){
        let rc:ReaderController = ReaderController()
       
        
        rc.save(post: self.post, indexPath: self.indexPath, sender: item )
    }
    
    
    func saveLike(item: FloatyItem){
        let strAction:String
        
        if (self.like == false){
            self.like = true
            strAction = "like"
            DispatchQueue.main.async {
                item.icon = #imageLiteral(resourceName: "ic_like_over")
            }
        }else{
            self.like = false
            strAction = "dislike"
            DispatchQueue.main.async {
                item.icon = #imageLiteral(resourceName: "ic_like")
            }
        }
        

        let lc:LikeController = LikeController.init(post: self.post)
        lc.sendLike(type: strAction, indexPath: self.indexPath, sender: item)
        
    }
    
    
    @objc func LikeChangeNotification(notification: NSNotification){
        
        if (notification.userInfo?["message"] as? String) != nil {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "msgInteractionNotification"), object: nil, userInfo: notification.userInfo )
        }
    }
    
    @objc func ReaderChangeNotification(notification: NSNotification){
        checkReaderState()
    }
    

 
}
