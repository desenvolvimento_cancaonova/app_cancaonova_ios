//
//  likeDAO.swift
//  cancaonova
//
//  Created by Desenvolvimento on 23/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import CouchbaseLite

class ReaderDAO: BaseDAO {
    
    init(){
        super.init(dataBase: "reader")
    }
    
    func addReader(post: Post)->Bool{
  
        return super.insert(model: post  )
    }
 
        
    func count()->Int{
        
        let query =  try!  CBLQueryBuilder (database: database, select: ["id"],where: "1=1", orderBy: ["-date"])
        
        let queryObj  = query.createQuery(withContext: nil)        
        let result = try! queryObj.run()

        return  Int(result.count)
        
    }
    
    
    func deleteOldDocuments(){
        let query =  try!  CBLQueryBuilder (database: database, select: ["id"], where: "delete = 1", orderBy: ["-date"])
        
        let queryObj  = query.createQuery(withContext: nil)
        
        let result = try! queryObj.run()
        
        for i in 0..<result.count{
            let row:CBLQueryRow = result.row(at: UInt(i))
            do{
                try row.document?.delete()
            }catch{
            }
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuBadgeChange"), object: nil, userInfo: nil)
        
    }
    
       
}
