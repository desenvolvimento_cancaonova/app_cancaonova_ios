//
//  dao.swift
//  cancaonova
//
//  Created by Desenvolvimento on 23/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import CouchbaseLite

class BaseDAO {
    
    var dataBaseName:String = ""
    var database:CBLDatabase!
    
    
    private func createConnection(){
        let manager = CBLManager.sharedInstance()
        do{
            database = try manager.databaseNamed(self.dataBaseName)
        } catch{
            print("Fail to create connection")
        }
    }
        
    func closeConnection(){
        do {
            try   database.close()
        } catch  {
            print("close error")
        }
    }
    
    init(dataBase:String){
        self.dataBaseName = dataBase
        self.createConnection()
    }
    
    func delete(model: Post)->Bool{
        let doc = self.database.document(withID: (model.document?.documentID)!)
       
        do{
         try doc?.delete()
            return true
        }catch{
            return false
        }    
    }
    
   
    internal func insert(model: Post)->Bool{
        let document = self.database.document(withID: (model.document?.documentID)!)
        let properties: [String : Any] = ["date" : NSDate().timeIntervalSince1970 ]
        
        do {
            try document?.putProperties(properties)
            return true
        }catch is NSException {
           
            return false
        }catch{
             return false
        }
    }
    
    
    func select( documentID:String )->CBLDocument?{
        let doc = database.document(withID:  documentID)
        return doc
    }
    
    func isActived(model:Post)->Bool{
        
        let doc = database.document(withID: (model.document?.documentID)!)
        
        if(doc?.properties == nil){
            return false
        }else{
            return true
        }
    }
 
    func  select( lastKey:Any!)-> CBLQueryEnumerator {
        let query =  try!  CBLQueryBuilder (database: database, select: ["id","title", "content","date","action", "type", "widget", "source", "source_slug", "image_url"],where: "1=1", orderBy: ["-date"])
        
        let queryObj  = query.createQuery(withContext: nil)
        queryObj.limit = 200
        queryObj.inclusiveStart =  false
        queryObj.startKey = lastKey
        
        let result = try! queryObj.run()
        
        return result
    }


}
