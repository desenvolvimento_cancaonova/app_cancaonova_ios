 
 
 import Foundation
 
 import CouchbaseLite
 
 class BaseModel: CBLModel {
    
    @NSManaged var action: NSArray
    @NSManaged var date: Double
    @NSManaged var id: String
    func firstAction() -> String{
        
        if let action:NSDictionary = self.action.object(at: 0) as? NSDictionary{
            return action.object(forKey: "ios") as! String
        }else{
            return ""
        }
    }
 }
  
