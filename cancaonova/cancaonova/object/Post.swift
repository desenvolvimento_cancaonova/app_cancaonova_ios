//
//  Post.swift
//  cancaonova
//
//  Created by Desenvolvimento on 03/05/17.
//  Copyright © 2017 Canção Nova. All rights reserved.
//

import Foundation
import CouchbaseLite


class Post: CBLModel {
    
    @NSManaged var action: NSArray
    @NSManaged var source: String
    @NSManaged var widget: String
    @NSManaged var image_url: NSDictionary
    @NSManaged var title: String
    @NSManaged var content: String
    @NSManaged var date: Double
    @NSManaged var id: String
    @NSManaged var source_slug: String
    @NSManaged var additional_data: NSDictionary
    @NSManaged var link: String

    func firstAction() -> String{
    
        if let action:NSDictionary = self.action.object(at: 0) as? NSDictionary{
            return action.object(forKey: "ios") as! String
        }else{
            return ""
        }
    }
   
    func titleToShare()-> String{
        if(widget == "doacao"){
            return "Estamos em \(content)%, com sua ajuda a Canção Nova conseguirá alcançar os 100%. "
        }else{
            return title
        }
    }
    
    func actionToShare() ->String{
        
        var urlToShare : String = firstAction()
        
        if(widget == "app"){
            urlToShare = "https://itunes.apple.com/app/id\(firstAction().replacingOccurrences(of: "cn", with: ""))"
        }
        
        return urlToShare
    }
    
    func likes ()->Int{
        if let likes_count = additional_data.object(forKey: "total_likes"){
            return likes_count as! Int
        }else{
            return 0
        }
    }
    
    func addLike(like: Int){
        
        if(self.additional_data.count == 0){
            self.additional_data = ["total_likes": 0]
        }
        
        var total:Int = likes()
        total += like
        let additional:NSMutableDictionary = additional_data.mutableCopy() as! NSMutableDictionary
        additional.setValue(total, forKey: "total_likes")
   
        additional_data = additional
  
    }
    
    func removeLike( like:Int){
        
        var total:Int = likes()
        total -= like
        
        if(self.additional_data.count == 0){
            self.additional_data = ["total_likes": 0]
        }
        
        let additional:NSMutableDictionary = additional_data.mutableCopy() as! NSMutableDictionary
        additional.setValue(total, forKey: "total_likes")
        
        additional_data = additional
        
    }
}
